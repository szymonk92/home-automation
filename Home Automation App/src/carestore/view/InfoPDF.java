package carestore.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.LayoutStyle.ComponentPlacement;

import carestore.controller.ActionMainScreen;


public class InfoPDF extends JFrame {

	private JPanel contentPane;
	
	private JButton btnOpenPdf;
	private JButton btnNewButton;
	/**
	 * Create the frame.
	 */
	public InfoPDF() {
		setTitle("Info PDF");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblPdfHasBeen = new JLabel("PDF has been created");
		lblPdfHasBeen.setFont(new Font("Tahoma", Font.PLAIN, 21));
		
		btnNewButton = new JButton("OK, close this window.");
		
	
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		btnOpenPdf = new JButton("Open PDF");
		
		btnOpenPdf.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ActionMainScreen.openPDF();
				
			}
		});
	
		btnOpenPdf.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(45)
					.addComponent(lblPdfHasBeen)
					.addContainerGap(173, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnOpenPdf)
					.addPreferredGap(ComponentPlacement.RELATED, 102, Short.MAX_VALUE)
					.addComponent(btnNewButton)
					.addGap(22))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(67)
					.addComponent(lblPdfHasBeen)
					.addPreferredGap(ComponentPlacement.RELATED, 108, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton)
						.addComponent(btnOpenPdf))
					.addGap(21))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
