package carestore.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import carestore.network.ConnectionNetwork;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.UnknownHostException;

public class InfoConnection extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JButton btnConnect;
	private JButton btnCloseApplication;

	public JButton getBtnCloseApplication() {
		return btnCloseApplication;
	}

	public void setBtnCloseApplication(JButton btnCloseApplication) {
		this.btnCloseApplication = btnCloseApplication;
	}

	public JButton getBtnConnect() {
		return btnConnect;
	}

	/**
	 * Create the frame.
	 */
	public InfoConnection() {
		setTitle("Problems with connection");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					ConnectionNetwork.connectToServer();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnConnect.setFont(new Font("Tahoma", Font.PLAIN, 17));

		JLabel lblNewLabel = new JLabel(
				"<html>There is some problem with <b>connection</b>. <br><br>Please check if Phidget is connected <br>to the same network as this computer.</html>");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));

		btnCloseApplication = new JButton("Close application.");
		btnCloseApplication.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		btnCloseApplication.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
				
			}
		});
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane
				.setHorizontalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGap(40)
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addGap(11)
																		.addComponent(
																				btnCloseApplication)
																		.addGap(57)
																		.addComponent(
																				btnConnect))
														.addComponent(
																lblNewLabel,
																GroupLayout.PREFERRED_SIZE,
																336,
																GroupLayout.PREFERRED_SIZE))
										.addContainerGap(63, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(
				Alignment.TRAILING)
				.addGroup(
						gl_contentPane
								.createSequentialGroup()
								.addGap(40)
								.addComponent(lblNewLabel)
								.addPreferredGap(ComponentPlacement.RELATED,
										46, Short.MAX_VALUE)
								.addGroup(
										gl_contentPane
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(
														btnCloseApplication)
												.addComponent(btnConnect))
								.addGap(33)));
		contentPane.setLayout(gl_contentPane);
	}
}
