package carestore.view;

import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JRadioButton;
import javax.swing.SpinnerDateModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JPasswordField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.CardLayout;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;

import CAALHP.AppAdapter;
import carestore.AppImplementation;
import carestore.ClassPreamble;
import carestore.model.Model;

import javax.swing.JSpinner;

public class MainScreen {
	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")
	private final String AppTITLE = "Window automation";
	private ImageIcon img = new ImageIcon("tree.png");
	
	
	String myCurrentDir = System.getProperty("user.dir")
            + File.separator
            + System.getProperty("sun.java.command")
                    .substring(0, System.getProperty("sun.java.command").lastIndexOf("."))
                    .replace(".", File.separator);
	
	
	 private BufferedImage bufferedImage = ImageIO.read(new File("keyboard120.png")); 
	 
	 private BufferedImage bufImage = ImageIO.read(new File("logout2.png"));
	
	// Carestore stuff:
	private AppImplementation appImpl;
	private AppAdapter adapter;

	/**
	 * @throws IOException
	 * @wbp.parser.entryPoint
	 */
	public MainScreen(Model model) throws IOException {
		initialize();
		System.out.println(myCurrentDir+"\\keyboard120.png");
		 appImpl = new AppImplementation();
		 adapter = new AppAdapter("localhost", appImpl);
	}

	private JFrame frame;
	private CardLayout cardlayout = new CardLayout();

	private static final String HEADER = "Care Store";

	// Panel title
	private JLabel lblCareStore;
	private JButton btnDisplaykeyboard;
	private JButton btnLogOut;

	// Panel Button
	private JButton btnWindow;
	private JButton btnDoor;
	private JButton btnSchedule;
	private JButton btnSettings;
	private JButton btnAdmin;

	// --Panel Cards--
	// Panel Window
	private JLabel lblStatusWindow;
	private JLabel lblWindowPanel;
	private JButton btnOpenWindow;
	private JButton btnCloseWindow;
	private JButton btnStopWindow;

	// Panel Door
	private JButton btnOpenDoor;

	// Panel schedule
	private ButtonGroup groupRadioBtn;
	private JButton btnOk;
	private JRadioButton rdbtnClose;
	private JRadioButton rdbtnOpen;
	private JSpinner spinner;

	// Panel settings
	private JLabel lblServer;

	private JButton btnSendMessage;
	private JTextField textField;
	private JButton btnGetStatistic;
	private JButton btnOpenStatistic;

	// Panel Admin
	private JLabel lblLoginPanel;
	private JLabel lblLoginOrPassword;
	private JButton btnLogIn;
	private JTextField textFieldLogin;
	private JPasswordField passwordField;

	// Panels
	private JPanel panelCards;
	private JPanel panelTitle;
	private JPanel panelButtons;

	// SubPanels
	private JPanel panelDoor;
	private JPanel panelSettings;
	private JPanel panelWindow;

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @throws IOException
	 */
	private void initialize() throws IOException {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		frame = new JFrame(AppTITLE); // App title

		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize(); // Get
																		// screen
																		// size

		frame.setBounds(0, 0, screen.width, screen.height); // set full screen
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setUndecorated(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		frame.setIconImage(img.getImage()); // set ICON
		frame.setVisible(true);

		// frame.setBackground(SystemColor.activeCaption);
		// frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Panels
		panelTitle = new JPanel();
		panelCards = new JPanel();
		panelButtons = new JPanel();

		groupRadioBtn = new ButtonGroup();// Group for radio buttons

		// Title Panel
		lblCareStore = new JLabel(HEADER);
		lblCareStore.setFont(new Font("Tahoma", Font.PLAIN, 35));

	
		btnDisplaykeyboard = new JButton(new ImageIcon(bufferedImage)); //
											// create
											// button
		btnDisplaykeyboard.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnDisplaykeyboard.setBorder(BorderFactory.createEmptyBorder());
		btnDisplaykeyboard.setContentAreaFilled(false);

		btnLogOut = new JButton (new ImageIcon(bufImage));
		btnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnLogOut.setBorder(BorderFactory.createEmptyBorder());
		btnLogOut.setContentAreaFilled(false);
		btnLogOut.setVisible(false);

		btnLogOut.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnLogOut.setContentAreaFilled(false);
		btnLogOut.setBorder(BorderFactory.createEmptyBorder());

		// Panel title
		GroupLayout gl_panelTitle = new GroupLayout(panelTitle);
		gl_panelTitle.setHorizontalGroup(gl_panelTitle.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelTitle
						.createSequentialGroup()
						.addGap(103)
						.addComponent(lblCareStore, GroupLayout.PREFERRED_SIZE,
								163, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED, 439,
								Short.MAX_VALUE)
						.addComponent(btnDisplaykeyboard,
								GroupLayout.PREFERRED_SIZE, 154,
								GroupLayout.PREFERRED_SIZE)
						.addGap(183)
						.addComponent(btnLogOut, GroupLayout.PREFERRED_SIZE,
								91, GroupLayout.PREFERRED_SIZE).addGap(38)));
		gl_panelTitle
				.setVerticalGroup(gl_panelTitle
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelTitle
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panelTitle
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelTitle
																		.createSequentialGroup()
																		.addComponent(
																				lblCareStore,
																				GroupLayout.PREFERRED_SIZE,
																				49,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(49))
														.addGroup(
																gl_panelTitle
																		.createSequentialGroup()
																		.addComponent(
																				btnDisplaykeyboard,
																				GroupLayout.DEFAULT_SIZE,
																				124,
																				Short.MAX_VALUE)
																		.addGap(21))))
						.addGroup(
								gl_panelTitle
										.createSequentialGroup()
										.addGap(21)
										.addComponent(btnLogOut,
												GroupLayout.PREFERRED_SIZE, 99,
												GroupLayout.PREFERRED_SIZE)
										.addContainerGap(36, Short.MAX_VALUE)));
		panelTitle.setLayout(gl_panelTitle);

		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																panelCards, 0,
																0,
																Short.MAX_VALUE)
														.addGroup(
																Alignment.TRAILING,
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				panelTitle,
																				GroupLayout.PREFERRED_SIZE,
																				1171,
																				GroupLayout.PREFERRED_SIZE)))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(panelButtons,
												GroupLayout.DEFAULT_SIZE, 185,
												Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addGap(11)
																		.addComponent(
																				panelButtons,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				panelTitle,
																				GroupLayout.PREFERRED_SIZE,
																				156,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				panelCards,
																				GroupLayout.PREFERRED_SIZE,
																				558,
																				GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(48, Short.MAX_VALUE)));
		panelCards.setLayout(cardlayout);

		panelWindow = new JPanel();
		panelCards.add(panelWindow, "panelWindow");

		lblWindowPanel = new JLabel("Window panel");
		lblWindowPanel.setFont(new Font("Tahoma", Font.PLAIN, 21));

		lblStatusWindow = new JLabel("Status");
		lblStatusWindow.setFont(new Font("Tahoma", Font.BOLD, 19));

		btnCloseWindow = new JButton("Close Window");
		btnCloseWindow.setFont(new Font("Tahoma", Font.PLAIN, 19));

		btnOpenWindow = new JButton("Open Window");
		btnOpenWindow.setFont(new Font("Tahoma", Font.PLAIN, 19));

		btnStopWindow = new JButton("Stop Window");
		btnStopWindow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnStopWindow.setFont(new Font("Tahoma", Font.PLAIN, 19));

		// Panel Window
		GroupLayout gl_panelWindow = new GroupLayout(panelWindow);
		gl_panelWindow
				.setHorizontalGroup(gl_panelWindow
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelWindow
										.createSequentialGroup()
										.addGap(75)
										.addGroup(
												gl_panelWindow
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelWindow
																		.createSequentialGroup()
																		.addComponent(
																				lblWindowPanel)
																		.addGap(265)
																		.addComponent(
																				btnStopWindow,
																				GroupLayout.DEFAULT_SIZE,
																				140,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelWindow
																		.createSequentialGroup()
																		.addGroup(
																				gl_panelWindow
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								btnCloseWindow,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								263,
																								Short.MAX_VALUE)
																						.addComponent(
																								btnOpenWindow,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								263,
																								Short.MAX_VALUE))
																		.addGap(214)
																		.addComponent(
																				lblStatusWindow)))
										.addGap(568)));
		gl_panelWindow.setVerticalGroup(gl_panelWindow
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panelWindow
								.createSequentialGroup()
								.addGap(67)
								.addComponent(lblWindowPanel)
								.addGap(73)
								.addComponent(btnOpenWindow,
										GroupLayout.PREFERRED_SIZE, 118,
										GroupLayout.PREFERRED_SIZE)
								.addGap(42)
								.addComponent(btnCloseWindow,
										GroupLayout.DEFAULT_SIZE, 107,
										Short.MAX_VALUE).addGap(125))
				.addGroup(
						gl_panelWindow
								.createSequentialGroup()
								.addGap(41)
								.addComponent(btnStopWindow,
										GroupLayout.PREFERRED_SIZE, 71,
										GroupLayout.PREFERRED_SIZE).addGap(108)
								.addComponent(lblStatusWindow)
								.addContainerGap(315, Short.MAX_VALUE)));
		panelWindow.setLayout(gl_panelWindow);

		JPanel panelSchedule = new JPanel();
		panelCards.add(panelSchedule, "panelSchedule");

		rdbtnOpen = new JRadioButton("Open");
		rdbtnOpen.setFont(new Font("Tahoma", Font.PLAIN, 19));
		groupRadioBtn.add(rdbtnOpen);
		rdbtnClose = new JRadioButton("Close");
		rdbtnClose.setFont(new Font("Tahoma", Font.PLAIN, 19));
		rdbtnClose.setSelected(true); // close default! for safety
		groupRadioBtn.add(rdbtnClose);

		JLabel lblSetWindowTime = new JLabel("Set window: ");
		lblSetWindowTime.setFont(new Font("Tahoma", Font.PLAIN, 21));

		btnOk = new JButton("OK");
		btnOk.setFont(new Font("Tahoma", Font.PLAIN, 19));

		Date date = new Date();
		SpinnerDateModel sm = new SpinnerDateModel(date, null, null,
				Calendar.MINUTE);

		spinner = new JSpinner(sm);
		spinner.setEditor(new JSpinner.DateEditor(spinner, "HH:mm"));
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 18));

		// Panel Schedule
		GroupLayout gl_panelSchedule = new GroupLayout(panelSchedule);
		gl_panelSchedule
				.setHorizontalGroup(gl_panelSchedule
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelSchedule
										.createSequentialGroup()
										.addGroup(
												gl_panelSchedule
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelSchedule
																		.createSequentialGroup()
																		.addGap(87)
																		.addGroup(
																				gl_panelSchedule
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								spinner,
																								GroupLayout.PREFERRED_SIZE,
																								281,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								rdbtnOpen)
																						.addComponent(
																								rdbtnClose)
																						.addComponent(
																								lblSetWindowTime)))
														.addGroup(
																gl_panelSchedule
																		.createSequentialGroup()
																		.addGap(124)
																		.addComponent(
																				btnOk,
																				GroupLayout.PREFERRED_SIZE,
																				100,
																				GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(808, Short.MAX_VALUE)));
		gl_panelSchedule.setVerticalGroup(gl_panelSchedule.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelSchedule
						.createSequentialGroup()
						.addGap(57)
						.addComponent(lblSetWindowTime)
						.addGap(41)
						.addComponent(rdbtnOpen)
						.addGap(18)
						.addComponent(rdbtnClose)
						.addGap(18)
						.addComponent(spinner, GroupLayout.PREFERRED_SIZE, 143,
								GroupLayout.PREFERRED_SIZE)
						.addGap(44)
						.addComponent(btnOk, GroupLayout.PREFERRED_SIZE, 70,
								GroupLayout.PREFERRED_SIZE)
						.addContainerGap(79, Short.MAX_VALUE)));
		panelSchedule.setLayout(gl_panelSchedule);
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		panelDoor = new JPanel();
		panelCards.add(panelDoor, "panelDoor");

		btnOpenDoor = new JButton("Open");
		btnOpenDoor.setFont(new Font("Tahoma", Font.PLAIN, 19));
		btnOpenDoor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GroupLayout gl_panelDoor = new GroupLayout(panelDoor);
		gl_panelDoor.setHorizontalGroup(gl_panelDoor.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelDoor
						.createSequentialGroup()
						.addGap(102)
						.addComponent(btnOpenDoor, GroupLayout.PREFERRED_SIZE,
								243, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(831, Short.MAX_VALUE)));
		gl_panelDoor.setVerticalGroup(gl_panelDoor.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelDoor
						.createSequentialGroup()
						.addGap(103)
						.addComponent(btnOpenDoor, GroupLayout.PREFERRED_SIZE,
								149, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(306, Short.MAX_VALUE)));

		panelDoor.setLayout(gl_panelDoor);

		JPanel panelAdmin = new JPanel();
		panelCards.add(panelAdmin, "panelAdmin");

		textFieldLogin = new JTextField();
		textFieldLogin.setFont(new Font("Tahoma", Font.PLAIN, 17));
		textFieldLogin.setColumns(10);

		btnLogIn = new JButton("Log in");
		btnLogIn.setFont(new Font("Tahoma", Font.PLAIN, 17));

		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 17));

		lblLoginOrPassword = new JLabel("");
		lblLoginOrPassword.setFont(new Font("Tahoma", Font.PLAIN, 21));

		lblLoginPanel = new JLabel("Login panel");
		lblLoginPanel.setFont(new Font("Tahoma", Font.PLAIN, 17));

		// Panel Admin
		GroupLayout gl_panelAdmin = new GroupLayout(panelAdmin);
		gl_panelAdmin
				.setHorizontalGroup(gl_panelAdmin
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								Alignment.TRAILING,
								gl_panelAdmin
										.createSequentialGroup()
										.addGap(108)
										.addGroup(
												gl_panelAdmin
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelAdmin
																		.createSequentialGroup()
																		.addGroup(
																				gl_panelAdmin
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								btnLogIn,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								200,
																								Short.MAX_VALUE)
																						.addComponent(
																								passwordField,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								200,
																								Short.MAX_VALUE)
																						.addComponent(
																								textFieldLogin,
																								GroupLayout.DEFAULT_SIZE,
																								200,
																								Short.MAX_VALUE))
																		.addGap(45))
														.addGroup(
																gl_panelAdmin
																		.createSequentialGroup()
																		.addComponent(
																				lblLoginPanel)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)))
										.addComponent(lblLoginOrPassword,
												GroupLayout.PREFERRED_SIZE,
												350, GroupLayout.PREFERRED_SIZE)
										.addGap(473)));
		gl_panelAdmin
				.setVerticalGroup(gl_panelAdmin
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelAdmin
										.createSequentialGroup()
										.addGap(80)
										.addGroup(
												gl_panelAdmin
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panelAdmin
																		.createSequentialGroup()
																		.addComponent(
																				lblLoginOrPassword,
																				GroupLayout.PREFERRED_SIZE,
																				118,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(71))
														.addGroup(
																Alignment.TRAILING,
																gl_panelAdmin
																		.createSequentialGroup()
																		.addGap(17)
																		.addComponent(
																				lblLoginPanel)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				textFieldLogin,
																				GroupLayout.PREFERRED_SIZE,
																				58,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(35)))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(passwordField,
												GroupLayout.PREFERRED_SIZE, 57,
												GroupLayout.PREFERRED_SIZE)
										.addGap(32)
										.addComponent(btnLogIn,
												GroupLayout.PREFERRED_SIZE, 61,
												GroupLayout.PREFERRED_SIZE)
										.addContainerGap(139, Short.MAX_VALUE)));
		panelAdmin.setLayout(gl_panelAdmin);

		panelSettings = new JPanel();
		panelCards.add(panelSettings, "panelSettings");

		textField = new JTextField();
		textField.setColumns(10);

		btnSendMessage = new JButton("Send");
		btnSendMessage.setFont(new Font("Tahoma", Font.PLAIN, 17));

		JLabel lblMessageToServer = new JLabel("Message to Server:");
		lblMessageToServer.setFont(new Font("Tahoma", Font.PLAIN, 17));

		btnGetStatistic = new JButton("Get activity data");
		btnGetStatistic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGetStatistic.setFont(new Font("Tahoma", Font.PLAIN, 17));

		lblServer = new JLabel("Server");
		lblServer.setFont(new Font("Tahoma", Font.PLAIN, 21));

		btnOpenStatistic = new JButton("View activity data");
		btnOpenStatistic.setFont(new Font("Tahoma", Font.PLAIN, 17));

		// Panel Settings
		GroupLayout gl_panelSettings = new GroupLayout(panelSettings);
		gl_panelSettings
				.setHorizontalGroup(gl_panelSettings
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelSettings
										.createSequentialGroup()
										.addGroup(
												gl_panelSettings
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelSettings
																		.createSequentialGroup()
																		.addGap(80)
																		.addGroup(
																				gl_panelSettings
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_panelSettings
																										.createSequentialGroup()
																										.addComponent(
																												textField,
																												GroupLayout.PREFERRED_SIZE,
																												211,
																												GroupLayout.PREFERRED_SIZE)
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addComponent(
																												btnSendMessage,
																												GroupLayout.PREFERRED_SIZE,
																												106,
																												GroupLayout.PREFERRED_SIZE))
																						.addComponent(
																								lblMessageToServer)
																						.addGroup(
																								gl_panelSettings
																										.createParallelGroup(
																												Alignment.TRAILING,
																												false)
																										.addComponent(
																												btnOpenStatistic,
																												Alignment.LEADING,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												Short.MAX_VALUE)
																										.addComponent(
																												btnGetStatistic,
																												Alignment.LEADING,
																												GroupLayout.DEFAULT_SIZE,
																												168,
																												Short.MAX_VALUE))))
														.addGroup(
																gl_panelSettings
																		.createSequentialGroup()
																		.addGap(129)
																		.addComponent(
																				lblServer)))
										.addGap(773)));
		gl_panelSettings
				.setVerticalGroup(gl_panelSettings
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelSettings
										.createSequentialGroup()
										.addGap(46)
										.addComponent(lblServer)
										.addGap(43)
										.addComponent(lblMessageToServer)
										.addGap(18)
										.addGroup(
												gl_panelSettings
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																btnSendMessage,
																GroupLayout.DEFAULT_SIZE,
																53,
																Short.MAX_VALUE)
														.addComponent(
																textField,
																GroupLayout.PREFERRED_SIZE,
																54,
																GroupLayout.PREFERRED_SIZE))
										.addGap(45)
										.addComponent(btnGetStatistic,
												GroupLayout.PREFERRED_SIZE, 60,
												GroupLayout.PREFERRED_SIZE)
										.addGap(18)
										.addComponent(btnOpenStatistic,
												GroupLayout.PREFERRED_SIZE, 60,
												GroupLayout.PREFERRED_SIZE)
										.addGap(140)));

		panelSettings.setLayout(gl_panelSettings);

		btnWindow = new JButton("Window");
		btnWindow.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnWindow.setPreferredSize(new Dimension(100, 100));

		// Buttons panel

		btnDoor = new JButton("Open Door");
		btnDoor.setForeground(Color.DARK_GRAY);
		btnDoor.setFont(new Font("Tahoma", Font.PLAIN, 17));

		btnAdmin = new JButton("Login");
		btnAdmin.setForeground(Color.DARK_GRAY);
		btnAdmin.setFont(new Font("Tahoma", Font.PLAIN, 17));

		btnSettings = new JButton("Settings");
		btnSettings.setVisible(false);
		btnSettings.setFont(new Font("Tahoma", Font.PLAIN, 17));

		btnSchedule = new JButton("Monitor window");
		btnSchedule.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});

		// Schedule
		btnSchedule.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout gl_panelButtons = new GroupLayout(panelButtons);
		gl_panelButtons
				.setHorizontalGroup(gl_panelButtons
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelButtons
										.createSequentialGroup()
										.addGap(24)
										.addGroup(
												gl_panelButtons
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelButtons
																		.createSequentialGroup()
																		.addComponent(
																				btnWindow,
																				GroupLayout.DEFAULT_SIZE,
																				151,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																gl_panelButtons
																		.createSequentialGroup()
																		.addComponent(
																				btnSchedule,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																gl_panelButtons
																		.createSequentialGroup()
																		.addComponent(
																				btnDoor,
																				GroupLayout.DEFAULT_SIZE,
																				151,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																gl_panelButtons
																		.createSequentialGroup()
																		.addComponent(
																				btnAdmin,
																				GroupLayout.DEFAULT_SIZE,
																				171,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																gl_panelButtons
																		.createSequentialGroup()
																		.addComponent(
																				btnSettings,
																				GroupLayout.DEFAULT_SIZE,
																				151,
																				Short.MAX_VALUE)
																		.addGap(30)))));
		gl_panelButtons.setVerticalGroup(gl_panelButtons.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelButtons
						.createSequentialGroup()
						.addGap(173)
						.addComponent(btnWindow, GroupLayout.PREFERRED_SIZE,
								109, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(btnSchedule, GroupLayout.PREFERRED_SIZE,
								116, GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(btnDoor, GroupLayout.PREFERRED_SIZE, 114,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(btnAdmin, GroupLayout.PREFERRED_SIZE,
								112, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnSettings, GroupLayout.PREFERRED_SIZE,
								102, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(19, Short.MAX_VALUE)));
		panelButtons.setLayout(gl_panelButtons);
		frame.getContentPane().setLayout(groupLayout);
	}

	public JSpinner getSpinner() {
		return spinner;
	}

	public JButton getBtnWindowPanel() {
		return btnWindow;
	}

	public JPanel getPanelCards() {
		return panelCards;
	}

	public JButton getBtnGetStatistic() {
		return btnGetStatistic;
	}

	public CardLayout getCardlayout() {
		return cardlayout;
	}

	// Get:
	public JButton getBtnSettings() {
		return btnSettings;
	}

	public JButton getBtnSchedule() {
		return btnSchedule;
	}

	public JButton getBtnAdmin() {
		return btnAdmin;
	}

	public JPanel getPanelDoor() {
		return panelDoor;
	}

	public JButton getBtnDoorPanel() {
		return btnDoor;
	}

	public JButton getBtnDisplaykeyboard() {
		return btnDisplaykeyboard;
	}

	public JLabel getLblLoginOrPassword() {
		return lblLoginOrPassword;
	}

	public JButton getBtnLogIn() {
		return btnLogIn;
	}

	public JButton getBtnSendMessage() {
		return btnSendMessage;
	}

	public JLabel getLblStream() {
		return lblWindowPanel;
	}

	public JTextField getTextFieldLogin() {
		return textFieldLogin;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public JButton getBtnOpenDoor() {
		return btnOpenDoor;
	}

	public JButton getBtnLogOut() {
		return btnLogOut;
	}

	public JLabel getLblStatusWindow() {
		return lblStatusWindow;
	}

	public JRadioButton getRdbtnOpen() {
		return rdbtnOpen;
	}

	public JRadioButton getRdbtnClose() {
		return rdbtnClose;
	}

	public JTextField getTextMesage() {
		return textField;
	}

	public JButton getBtnCloseWindow() {
		return btnCloseWindow;
	}

	public JButton getBtnOpenWindow() {
		return btnOpenWindow;
	}

	public JButton getBtnOk() {
		return btnOk;
	}

	public JFrame getFrame() {
		return frame;
	}

	public JPanel getPanelWindow() {
		return panelWindow;
	}

	public JButton getBtnOpenStatistic() {
		return btnOpenStatistic;
	}

	public JButton getBtnStopWindow() {
		return btnStopWindow;
	}

}
