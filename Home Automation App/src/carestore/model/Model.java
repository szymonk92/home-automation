package carestore.model;

import carestore.ClassPreamble;

public class Model {
	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")
	private static boolean connection;
	private static boolean windowObstacle = false;
	private static boolean inTheMiddle = false;
	private static boolean windowOpenStatus;
	private static boolean infoConnectionProblemShow;

	public Model() {
		super();
	}

	public static boolean isWindowOpenStatus() {
		return windowOpenStatus;
	}

	public static void setWindowOpenStatus(boolean windowOpenStatus) {
		Model.windowOpenStatus = windowOpenStatus;
	}

	public static boolean isWindowObstacle() {
		return windowObstacle;
	}

	public static void setWindowObstacle(boolean windowObstacle) {
		Model.windowObstacle = windowObstacle;
	}

	public static boolean isInTheMiddle() {
		return inTheMiddle;
	}

	public static void setInTheMiddle(boolean inTheMiddle) {
		Model.inTheMiddle = inTheMiddle;
	}

	public static boolean isConnection() {
		return connection;
	}

	public static void setConnection(boolean connection) {
		Model.connection = connection;
	}

	public static boolean isInfoConnectionProblemShow() {
		return infoConnectionProblemShow;
	}

	public static  void setInfoConnectionProblemShow(boolean infoConnectionProblemShow) {
		Model.infoConnectionProblemShow = infoConnectionProblemShow;
	}

}
