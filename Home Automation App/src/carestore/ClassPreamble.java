package carestore;

public @interface ClassPreamble {

	String author() default "Szymon Klepacz"; 
	String date();
	int currentRevision() default 1;
	String lastModified() default "N/A";
	String lastModifiedBy() default "N/A";

}