package carestore;

import java.awt.EventQueue;

import carestore.controller.ActionMainScreen;
import carestore.model.Model;
import carestore.view.MainScreen;

public class Main {
    
	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Model model = new Model();
					MainScreen window = new MainScreen(model);
					ActionMainScreen actionMainScreen = new ActionMainScreen(
							model, window);

					Model.setConnection(false);
					actionMainScreen.connectToServer();
					actionMainScreen.contol(); 
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}

