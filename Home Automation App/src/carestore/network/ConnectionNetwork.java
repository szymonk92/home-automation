package carestore.network;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.SwingUtilities;

import carestore.ClassPreamble;
import carestore.controller.ActionMainScreen;
import carestore.controller.PopingWindows;
import carestore.model.Model;
import carestore.view.InfoConnection;

public class ConnectionNetwork {

	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")
	private final static String serverName = "phidgetsbc.local.";
	private final static int portNumber = 5100;
	private static Socket socket;
	private static PrintWriter printWriter;
	private static BufferedReader bufferedReader;
	private static String messageFromServer;

	private static BufferedOutputStream bufferedOutputStream = null;
	private final static String FILE_TO_RECEIVED = "Stat.pdf";
	private final static int FILE_SIZE = 60223806;
	private static byte[] mybytearray = new byte[FILE_SIZE];
	private static int bytesRead;
	private static int current = 0;
	static InfoConnection frame = new InfoConnection();

	public static void connectToServer() throws UnknownHostException,
			IOException {

		try {

			socket = new Socket(serverName, portNumber);

			printWriter = new PrintWriter(new OutputStreamWriter(
					socket.getOutputStream()));

			bufferedReader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));

			Model.setConnection(true);
			System.out.println("CONNECTED");

		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: hostname");
		} catch (IOException e) {
			System.err
					.println("Couldn't get I/O for the connection to: hostname");
		}
		readMessage();

	}

	public void sendMessage(String text) {
		if (socket != null && printWriter != null && bufferedReader != null) {

			printWriter.println(text);
			printWriter.flush();
		}
	}

	public static void showNotConnected() {
		try {

			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {

					if (Model.isInfoConnectionProblemShow() == false) {
						frame.setVisible(false);
						frame.setVisible(true);
						System.out.println("DUPA");
						Model.setInfoConnectionProblemShow(true);
					} else {
						System.out.println("ELSE");
					}

				}

			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void readMessage() {

		Runnable readTask = new Runnable() {

			@Override
			public void run() {
				try {
					if (socket != null && printWriter != null
							&& bufferedReader != null) {
						printWriter.println("Hello Server!");
						printWriter.flush();

					}
					// Messages from server
					while ((messageFromServer = bufferedReader.readLine()) != null) {
						Model.setConnection(true);
						frame.setVisible(false);

						System.out.println("Server: " + messageFromServer);
						if (messageFromServer.indexOf("Close") != -1) {
							bufferedReader.close();
							printWriter.close();
							socket.close();
							System.out.println("Socket has been closed");
						}
						if (messageFromServer.indexOf("Connected") != -1) {
							System.out.println("Connected");
						}
						if (messageFromServer.indexOf("Middle") != -1) {
							Model.setInTheMiddle(true);
							ActionMainScreen.windowStatusMiddle();
							System.out.println("In the middle of nowhere");
						}
						if (messageFromServer.indexOf("NoDle") != -1) {
							Model.setInTheMiddle(false);
							System.out.println("Not in the middle");
						}
						if (messageFromServer.indexOf("WindowObstacle") != -1) {
							Model.setWindowObstacle(true);
							ActionMainScreen.windowStatusObstacle();
							System.out.println("Obstacle detected");
						}
						if (messageFromServer.indexOf("WindowOk") != -1) {
							Model.setWindowObstacle(false);
							System.out.println("No obstacle");
						}
						
						if (messageFromServer.indexOf("WindowAlreadyOpened") != -1) {
							Model.setWindowObstacle(false);
							ActionMainScreen.windowStatusAlreadyOpened();
							System.out.println("Window already opened");
						}
						
						if (messageFromServer.indexOf("WindowAlreadyShutDown") != -1) {//be carfull, not the word Close
							Model.setWindowObstacle(false);
							ActionMainScreen.windowStatusAlreadyClosed();
							System.out.println("Window already closed");
						}

						if (messageFromServer.indexOf("WindowStopped") != -1) {// if
																				// (messageFromServer.indexOf("Window Closed")
																				// !=
																				// -1)
																				// {
							Model.setWindowOpenStatus(false);
							ActionMainScreen.windowStatusClosed();
							System.out.println("Window closed");
						}

						if (messageFromServer.indexOf("WindowOpened") != -1) {
							Model.setWindowOpenStatus(true);
							ActionMainScreen.windowStatusOpened();
							System.out.println("Window opened");
						}

						if (messageFromServer.indexOf("PDF") != -1) {

							System.out.println("recieving the PDF");

							bufferedOutputStream = new BufferedOutputStream(
									new FileOutputStream(FILE_TO_RECEIVED));
							current = 0;

							InputStream is = ConnectionNetwork.socket
									.getInputStream();

							bytesRead = is.read(mybytearray, 0,
									mybytearray.length);
							current = bytesRead;

							do {
								bytesRead = is.read(mybytearray, current,
										(mybytearray.length - current));
								if (bytesRead >= 0) {
									current += bytesRead;
								}
							} while (bytesRead > -1);

							bufferedOutputStream.write(mybytearray, 0, current);
							bufferedOutputStream.flush();
							System.out.println("File " + FILE_TO_RECEIVED
									+ " downloaded (" + current
									+ " bytes read)");

							if (bufferedOutputStream != null)
								bufferedOutputStream.close();

							System.out.println("bufferedOutputStream closed");
							bufferedReader.close();
							printWriter.close();
							socket.close();

							connectToServer();
							PopingWindows.showInfoPDF(); 
							break;
						}
					}

				} catch (java.net.SocketException e) {
					e.printStackTrace();
					Model.setConnection(false);
					showNotConnected();
				} catch (IOException e) {
					e.printStackTrace();
					Model.setConnection(false);
					showNotConnected();
				} catch (Exception e) {
					e.printStackTrace();
					Model.setConnection(false);
					showNotConnected();
				}

			}
		};
		Thread readThread = new Thread(readTask);
		readThread.start();
	}
}
