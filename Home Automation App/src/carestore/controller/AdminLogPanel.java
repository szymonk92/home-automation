package carestore.controller;

import carestore.ClassPreamble;
import carestore.model.ModelAdmin;

class AdminLogPanel {
	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")
	


	public int checkIfAdmin(String login, String password) {

		if (ModelAdmin.getLogin().equals(login)
				&& ModelAdmin.getPassword().equals(password)) {
			return 1;
		} else {
			return 0;
		}
	}
}
