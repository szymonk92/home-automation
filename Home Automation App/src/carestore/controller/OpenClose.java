package carestore.controller;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import carestore.ClassPreamble;
import carestore.model.Model;

class OpenClose {
	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")
	private Model model = new Model();

	/**
	 * close - false open - true
	 * 
	 * @param doorWindows
	 */

	public OpenClose() {
	}

	void closeAfter(final int delay) {
		ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);

		exec.schedule(new Runnable() {
			public void run() {

				System.out.println("Closed after: " + delay);

			}
		}, delay, TimeUnit.MINUTES);
	}

	void openAfter(final int delay) {
		ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);

		exec.schedule(new Runnable() {
			public void run() {

				System.out.println("Opened after: " + delay);

			}
		}, delay, TimeUnit.MINUTES);
	}
}
