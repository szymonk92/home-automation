package carestore.controller;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import carestore.ClassPreamble;
import carestore.model.Model;
import carestore.network.ConnectionNetwork;

import carestore.view.MainScreen;

public class ActionMainScreen {
	@ClassPreamble(date = "17/5/2014", currentRevision = 1, lastModified = "17/5/2014", lastModifiedBy = "Szymon Klepacz")
	private static MainScreen viewMain;
	private ConnectionNetwork connectionNetwork = new ConnectionNetwork();
	private AdminLogPanel admPanel = new AdminLogPanel();
	private static String MSG;

	private int authResponse;
	private static final String CORRECTLOG = "Hello Admin";
	private static final String WRONGLOG = "Try again";

	public ActionMainScreen(Model model, MainScreen window) {
		ActionMainScreen.viewMain = window;
	}

	public void connectToServer() {

		try {
			ConnectionNetwork.connectToServer();

		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (java.net.ConnectException e1) {
			e1.printStackTrace();
			System.out.println("ConnectException");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static void openPDF() {

		if (Desktop.isDesktopSupported()) {
			try {
				File myFile = new File("Stat.pdf");

				Desktop.getDesktop().open(myFile);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("PDF programm is missing");
			} catch (java.lang.NullPointerException e) {
				e.printStackTrace();
				System.out.println("File doesn;t exist. Generate it first");
			}
		}

	}

	public static void windowStatusOpened() {

		viewMain.getLblStatusWindow().setText("Window open");
	}

	public static void windowStatusClosed() {
		viewMain.getLblStatusWindow().setText("Window close");
	}

	public static void windowStatusObstacle() {
		MSG = viewMain.getLblStatusWindow().getText();
		viewMain.getLblStatusWindow().setText("Obstacle detected");
		viewMain.getBtnCloseWindow().setEnabled(true);
		viewMain.getBtnOpenWindow().setEnabled(true);
	}
	
	
	public static void windowStatusAlreadyOpened() {
		viewMain.getLblStatusWindow().setText("Window already opened");
		viewMain.getBtnCloseWindow().setEnabled(true);
		viewMain.getBtnOpenWindow().setEnabled(false);
	}
	
	public static void windowStatusAlreadyClosed() {
		viewMain.getLblStatusWindow().setText("Window already closed");
		viewMain.getBtnCloseWindow().setEnabled(false);
		viewMain.getBtnOpenWindow().setEnabled(true);
	}

	public static void windowStatusMiddle() throws InterruptedException {
		if (Model.isInTheMiddle() == true) {
			MSG = viewMain.getLblStatusWindow().getText();
			viewMain.getLblStatusWindow().setText("Window is in the helf way.");
			Thread.sleep(1000);
			viewMain.getLblStatusWindow().setText(MSG);
		} else {
			viewMain.getLblStatusWindow().setText("It is ok!");
		}
	}

	public void contol() {

		// Windows
		viewMain.getBtnOpenWindow().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				connectionNetwork.sendMessage("OpenWindow");
				/*
				 * if (Model.isInTheMiddle() == true &&
				 * Model.isWindowOpenStatus() == true) {
				 * viewMain.getBtnOpenWindow().setEnabled(false);
				 * viewMain.getLblStatusWindow().setText("Window ope"); } else
				 * if(Model.isWindowOpenStatus() == false) {
				 */

				viewMain.getLblStatusWindow().setText("Opening ...");
				viewMain.getBtnCloseWindow().setEnabled(true);
				viewMain.getBtnOpenWindow().setEnabled(false);
				
				/*
				 * } else if (Model.isInTheMiddle() ==false &&
				 * Model.isWindowOpenStatus() == false) { windowStatusOpened();
				 * }
				 * 
				 * else { viewMain.getLblStatusWindow().setText("WTF?"); }
				 */
			}
		});

		viewMain.getBtnCloseWindow().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				connectionNetwork.sendMessage("CloseWindow");
				
				viewMain.getLblStatusWindow().setText("Closing ...");
				viewMain.getBtnOpenWindow().setEnabled(true);
				viewMain.getBtnCloseWindow().setEnabled(false);
				
			}
		});

		viewMain.getBtnStopWindow().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				connectionNetwork.sendMessage("StopWindow");
				viewMain.getBtnOpenWindow().setEnabled(true);
				viewMain.getBtnCloseWindow().setEnabled(true);
				viewMain.getLblStatusWindow().setText("Window stoped");
			}
		});

		// Settings

		viewMain.getBtnSendMessage().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String tmp = viewMain.getTextMesage().getText();
				connectionNetwork.sendMessage(tmp);
			}
		});

		// Schedule
		viewMain.getBtnOk().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Date date;
				date = (Date) viewMain.getSpinner().getValue();
				DateFormat dateFormat = new SimpleDateFormat("HH:mm");
				String dateFormated = dateFormat.format(date);

				if (viewMain.getRdbtnClose().isSelected()) {
					connectionNetwork.sendMessage("Date:"+dateFormated+":close");
					System.out.println("Date:" + dateFormated + ":close");
				} else {
					connectionNetwork.sendMessage("Date:"+dateFormated+":open");
					System.out.println("Date:" + dateFormated + ":open");
				}

			}
		});

		// Log In
		viewMain.getBtnLogIn().addActionListener(new ActionListener() {

			String login;
			String password;

			@Override
			public void actionPerformed(ActionEvent e) {

				login = viewMain.getTextFieldLogin().getText();
				password = new String(viewMain.getPasswordField().getPassword());
				authResponse = admPanel.checkIfAdmin(login, password);

				if (authResponse == 1) {
					viewMain.getLblLoginOrPassword().setText(CORRECTLOG);
					viewMain.getBtnSettings().setVisible(true);
					viewMain.getBtnAdmin().setVisible(false);
					viewMain.getBtnLogOut().setVisible(true);
					viewMain.getCardlayout().show(viewMain.getPanelCards(),
							"panelSettings");
					viewMain.getTextFieldLogin().setText("");
					viewMain.getPasswordField().setText("");

				} else {
					viewMain.getLblLoginOrPassword().setText(WRONGLOG);
				}

				viewMain.getLblLoginOrPassword().setVisible(true);
			}
		});

		viewMain.getBtnLogOut().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				viewMain.getLblLoginOrPassword().setText("Logged out");
				viewMain.getBtnSettings().setVisible(false);
				viewMain.getBtnAdmin().setVisible(true);
				viewMain.getBtnLogOut().setVisible(false);
				viewMain.getCardlayout().show(viewMain.getPanelCards(),
						"panelAdmin");

			}
		});

		// Keyboard
		viewMain.getBtnDisplaykeyboard().addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						try {

							Runtime.getRuntime().exec(
									"cmd /c C:\\Windows\\System32\\osk.exe");
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				});

		// Door
		viewMain.getBtnOpenDoor().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				connectionNetwork.sendMessage("OpenDoor");
			}
		});

		viewMain.getBtnGetStatistic().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				connectionNetwork.sendMessage("GeneratePDF");

			}
		});

		viewMain.getBtnOpenStatistic().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				openPDF();
			}
		});

		viewMain.getBtnDoorPanel().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				viewMain.getCardlayout().show(viewMain.getPanelCards(),
						"panelDoor");
			}
		});

		viewMain.getBtnSchedule().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				viewMain.getCardlayout().show(viewMain.getPanelCards(),
						"panelSchedule");
			}
		});

		viewMain.getBtnWindowPanel().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				viewMain.getCardlayout().show(viewMain.getPanelCards(),
						"panelWindow");
			}
		});

		viewMain.getBtnAdmin().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				viewMain.getCardlayout().show(viewMain.getPanelCards(),
						"panelAdmin");
			}
		});

		viewMain.getBtnSettings().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				viewMain.getCardlayout().show(viewMain.getPanelCards(),
						"panelSettings");
			}
		});

		
	}
}
